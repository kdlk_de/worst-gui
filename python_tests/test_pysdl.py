"""User interface examples."""
import sys
import sdl2
import sdl2.ext
RED = sdl2.ext.Color(255, 0, 0)
global button_list
button_list = []
# Create a resource, so we have easy access to the example images.
RESOURCES = sdl2.ext.Resources(__file__, "resources")

# A callback for the Button.click event.
def onclick(button, event):
    print("Button was clicked!", event)
    # sdl2.ext.quit()
    global running 
    running = False

def unhide_menu(button, event):
    color = RED
    exit_button2 = uifactory.from_image(sdl2.ext.BUTTON,
                                RESOURCES.get_path("close button.png"))
    global exit_button
    if exit_button in button_list:
        button_list.remove(exit_button)
    else:
        button_list.append(exit_button)
    # print(dir(button.surface))
    # button.subsprite = exit_button2.subsprite
    # sdl2.ext.fill(button.surface, color)
    # button.flip ^= sdl2.SDL_FLIP_VERTICAL
    # sprite.flip ^= sdl2.SDL_FLIP_VERTICAL

def run():
    # You know those from the helloworld.py example.
    # Initialize the video subsystem, create a window and make it visible.
    sdl2.ext.init()
    window = sdl2.ext.Window("UI Elements", size=(800, 600))
    window.show()

    # Create a sprite factory that allows us to create visible 2D elements
    # easily. Depending on what the user chosses, we either create a factory
    # that supports hardware-accelerated sprites or software-based ones.
    # The hardware-accelerated SpriteFactory requres a rendering context
    # (or SDL_Renderer), which will create the underlying textures for us.
    if "-hardware" in sys.argv:
        print("Using hardware acceleration")
        renderer = sdl2.ext.Renderer(window, flags=sdl2.render.SDL_RENDERER_ACCELERATED)
        factory = sdl2.ext.SpriteFactory(sdl2.ext.TEXTURE, renderer=renderer)
    else:
        print("Using software rendering")
        renderer = sdl2.ext.Renderer(window, flags=sdl2.render.SDL_RENDERER_SOFTWARE)
        factory = sdl2.ext.SpriteFactory(sdl2.ext.SOFTWARE)

    # Create a UI factory, which will handle several defaults for
    # us. Also, the UIFactory can utilises software-based UI elements as
    # well as hardware-accelerated ones; this allows us to keep the UI
    # creation code clean.
    global uifactory
    uifactory = sdl2.ext.UIFactory(factory)

    # Create a simple Button sprite, which reacts on mouse movements and
    # button presses and fill it with a white color. All UI elements
    # inherit directly from the TextureSprite (for TEXTURE) or SoftwareSprite
    # (for SOFTWARE), so everything you can do with those classes is also
    # possible for the UI elements.
    global exit_button
    exit_button = uifactory.from_image(sdl2.ext.BUTTON,
                                  RESOURCES.get_path("close button.png"))
    exit_button.position = 50, 50

    exit_button.click += onclick

    button2 = uifactory.from_image(sdl2.ext.BUTTON,
                                  RESOURCES.get_path("button down.png"))
    button2.position = 375, 0

    button2.click += unhide_menu

    
    button_list.append(button2)
    # Bind some actions to the entry's event handlers. The TextEntry
    # receives input events, once it has been activated by a mouse
    # button press on its designated area. The UIProcessor class takes
    # care of this internally through its activate() method.  If the
    # TextEntry is activated, SDL_TEXTINPUT events are enabled by the
    # relevant SDL2 functions, causing input events to occur, that are
    # handled by the TextEntry.
    # draw them on the Window.
    spriterenderer = factory.create_sprite_render_system(window)

    # Create a new UIProcessor, which will handle the user input events
    # and pass them on to the relevant user interface elements.
    uiprocessor = sdl2.ext.UIProcessor()
    global running
    running = True
    mouse_toggle = False
    while running:
        events = sdl2.ext.get_events()
        for event in events:
            if event.type == sdl2.SDL_QUIT:
                running = False
                break
            # Pass the SDL2 events to the UIProcessor, which takes care of
            # the user interface logic.

            if event.type == sdl2.SDL_MOUSEBUTTONDOWN:
                mouse_toggle = True
            elif event.type == sdl2.SDL_MOUSEBUTTONUP:
                mouse_toggle = False
                
            if mouse_toggle:
                button2.x = event.motion.x
                button2.y = event.motion.y
            # button2.x = event.motion.x
            # button2.y = event.motion.y
            uiprocessor.dispatch(button_list, event)
        renderer.clear(0)
        # Render all user interface elements on the window.
        spriterenderer.render(button_list)

    sdl2.ext.quit()
    return 0


if __name__ == "__main__":
    sys.exit(run())
