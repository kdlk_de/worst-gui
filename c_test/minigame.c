#include <stdlib.h>
#include <stdbool.h>

#include <SDL2/SDL.h>

#define WINDOW_WIDTH 300
// #define WINDOW_HEIGHT (WINDOW_WIDTH)
#define WINDOW_HEIGHT 300

struct icon {
   int *icon_address;
};
struct color_rect_node *add;

typedef struct color_rect {
    SDL_Rect rect;
    int r;
    int g;
    int b;
    struct color_rect * next;
} color_rect;

typedef struct color_rect_node {
    color_rect cRect;
    struct color_rect_node *next;
} color_rect_node;

typedef struct int_node {
    int val;
    struct int_node * next;
} int_node;

//color_rect_node first, last;

// color_rect_node create_new_rect_node(SDL_Rect rect, int r, int g, int b){
//     last.next = (color_rect_node *) malloc(sizeof(color_rect_node));
//     last.next->cRect = {{rect, r, g, b}, 0};
//     printf("func next %x\n", last.next);
//     return last.next.cRect;
// }
int_node *first, *last;
void add_int(int adder){
    last->next = (int_node *) malloc(sizeof(int_node));
    last->next->val = adder;
    last->next->next = 0;
    last = last->next;
}

color_rect *first_node, *last_node;
void add_node(SDL_Rect rect, int r, int g, int b){
    last_node->next = (color_rect *) malloc(sizeof(color_rect));
    last_node->next->rect = rect;
    last_node->next->r = r;
    last_node->next->g = g;
    last_node->next->b = b;
    last_node->next->next = 0;
    last_node = last_node->next;
}

int main(int argc, char **argv) {
    first_node = (color_rect *) malloc(sizeof(color_rect));
    SDL_Rect reccy= {50,50,50,50};
    int r = 12;
    int g = 12;
    int b = 12;
    first_node->rect = reccy;
    first_node->r = r;
    first_node->g = g;
    first_node->b = b;
    last_node = first_node;
    first_node->next = last_node;

    printf("%d\n", first_node->rect.h);
    printf("%d\n", last_node->rect.h);
    reccy.h = reccy.h+1;
    add_node(reccy, 13,13,13);
    printf("%d\n", last_node->rect.h);
    reccy.h = reccy.h+1;
    add_node(reccy, 14,14,14);
    printf("%d\n", last_node->rect.h);
    reccy.h = reccy.h+1;
    add_node(reccy, 15,15,15);
    printf("%d\n", last_node->rect.h);

    printf("first_next %x\n", first_node->next);
    printf("last_node_next %x\n", last_node->next);
    color_rect * current_pointer;

    for (current_pointer = first_node; current_pointer != 0; current_pointer = current_pointer->next){
        printf("current_pointer %d\n", current_pointer->rect.h);
    }

    // first = (int_node *) malloc(sizeof(int_node));
    // first->val = 4;
    // last = first;
    // first->next = last;

    // printf("%d\n", first->val);
    // printf("%d\n", last->val);
    // add_int(7);
    // printf("%d\n", last->val);
    // add_int(12);
    // printf("%d\n", last->val);
    // add_int(8);
    // printf("%d\n", last->val);

    // printf("first_next %x\n", first->next);
    // printf("last_next %x\n", last->next);
    // int_node * current_pointer;

    // for (current_pointer = first; current_pointer != 0; current_pointer = current_pointer->next){
    //     printf("current_pointer %d\n", current_pointer->val);
    // }


    // printf("%x\n", *(asd.icon_address));
    // printf("%x\n", asd.icon_address);
    // printf("%s\n", aa);
    // SDL_Rect reccy = {50,50,50,50};
    // // first = create_new_rect_node(reccy, 120, 120, 120);
    // first = (color_rect_node) {{{5,5,50,50}, 1, 1, 1}, 0};
    // last = first;
    // printf("%d\n", last.cRect.r);
    // printf("next %x\n", last.next);
    // printf("add %x\n", add);
    // color_rect_node second = create_new_rect_node(reccy, 122, 122, 122);
    // printf("add %x\n", add);
    // printf("ausgabe %d\n", add->cRect.rect.h);
    // printf("%d\n", last.cRect.r);
    // printf("next %x\n", last.next);
    // color_rect_node third = create_new_rect_node(reccy, 13, 123, 123);

    // // printf("%d\n", last.cRect.rect.x);
    // printf("%d\n", last.cRect.r);
    // printf("%x\n", last.next);
    // // printf("%d\n", last.cRect.g);
    // // printf("%d\n", last.cRect.b);
    // printf("%x\n", first.next);

    // color_rect_node current_pointer;

    // for(current_pointer = first; current_pointer.next != 0; current_pointer = *current_pointer.next){
    //     printf("loop %d\n", current_pointer.cRect.r);
    // }
    // int window_x, window_y;
    // SDL_Event event;
    // SDL_Renderer *renderer;
    // SDL_Window *window;
    // int quit;

    // /* Inint TTF. */
    // SDL_Init(SDL_INIT_VIDEO);
    // SDL_CreateWindowAndRenderer(WINDOW_WIDTH, WINDOW_WIDTH, 0, &window, &renderer);
    // SDL_GetWindowSize(window, &window_x, &window_y);
    // SDL_Log("Window Size x, y:  %d , %d", window_x, window_y);
    //     // get_text_and_rect(renderer, 10, 10, "hello, world - from clutter", font, &texture1, &rect1);
    // // combine_text(renderer, 10, 10, "hello, world", "hello! world", font, &texture1, &texture2, &rect1, &rect2);
    // //get_text_and_rect(renderer, 0, rect1.y + rect1.h, "world", font, &texture2, &rect2);

    // // Render the rect to the screen
    // SDL_RenderPresent(renderer);

    // quit = 0;
    // while (!quit) {
    //     while (SDL_PollEvent(&event) == 1) {
    //         if (event.type == SDL_QUIT) {
    //             quit = 1;
    //         }
    //     }

    //     // get cursor position relative to window
    //     SDL_SetRenderDrawColor(renderer, 255, 255, 255, 0);
    //     SDL_RenderClear(renderer);
    //     // SDL_RenderFillRect( renderer, &r );
    //     // r.y = 50;
    //     int x = 0;
    //     for(x=0;x < window_x; x++){
    //         // int rand1 = rand() % 256;
    //         // // int rand2 = rand1;
    //         // // int rand3 = rand1;
    //         // int rand2 = rand() % 256;
    //         // int rand3 = rand() % 256;
    //         int y = 0;
    //         SDL_Rect rect;
    //         for(y=0; y < window_y; y++){
    //             rect.y = y;
    //             rect.x = x;
    //             rect.h = 1;
    //             rect.w = 1;
    //             // SDL_SetRenderDrawColor(renderer, rand1, rand2, rand3, 0);
    //             SDL_SetRenderDrawColor(renderer, rand() % 256, rand() % 256, rand() % 256, 0);
    //             SDL_RenderFillRect( renderer, &rect );

    //         }
    //     }
    //     /*
        
    //             for(x=0;x < window_x; x++){
    //         int y = 0;

    //         for(y=0; y < window_y; y++){
    //             rect_array[y][x].y = y;
    //             rect_array[y][x].x = x;
    //             rect_array[y][x].h = 1;
    //             rect_array[y][x].w = 1;
    //             SDL_SetRenderDrawColor(renderer, 255, 120, 255, 0);
    //             SDL_RenderFillRect( renderer, &rect_array[y][x] );
    //         }
    //     }
        
    //     */
    //     //SDL_SetRenderDrawColor( renderer, 02, 130, 209, 255 );
    //     // Render rect
    //     // r.y = clipper.y + (clipper.h / 2);
    //     SDL_RenderPresent(renderer);
    //     // // wait 1/30th of a second
    //     // SDL_Delay(1000/30);
    //     SDL_Delay(1000/4);
    // }


    // SDL_DestroyRenderer(renderer);
    // SDL_DestroyWindow(window);
    // SDL_Quit();
    // return EXIT_SUCCESS;
}
