#include <stdlib.h>
#include <stdbool.h>

#include <SDL2/SDL.h>
// #include <SDL2/SDL_ttf.h>


#define WINDOW_WIDTH 300
#define WINDOW_HEIGHT (WINDOW_WIDTH)


#define NUMBER_OF_STRING 4
#define MAX_STRING_SIZE 40
/*
- x, y: upper left corner.
- texture, rect: outputs.
*/

// void combine_text(SDL_Renderer *renderer, int x, int y, char *text, char *text2,
//         TTF_Font *font, SDL_Texture **texture, SDL_Texture **texture2, SDL_Rect *rect, SDL_Rect *rect2){
//     int text_width;
//     int text_height;
//     SDL_Surface *surface;
//     SDL_Surface *surface2;
//     SDL_Color textColor = {255, 122, 255, 0};

//     surface = TTF_RenderText_Solid(font, text, textColor);
//     surface2 = TTF_RenderText_Solid(font, text2, textColor);
//      surface2->h *= 2;
//     rect2->x = 0;
//     rect2->y = 0 + surface->h;
//     rect2->h = surface->h + surface2->h;
//     SDL_BlitSurface(surface2, NULL, surface, NULL);
//     // SDL_LowerBlit(surface2, NULL, surface, NULL);
//     *texture = SDL_CreateTextureFromSurface(renderer, surface);
//     // *texture = SDL_CreateTextureFromSurface(renderer, surface2);
//     text_width = surface->w;
//     text_height = surface->h;
//     SDL_FreeSurface(surface);
//     SDL_FreeSurface(surface2);
//     rect->x = x;
//     rect->y = y;
//     rect->w = text_width;
//     rect->h = text_height;
// }


int main(int argc, char **argv) {
    int window_x, window_y;
    SDL_Event event;
    SDL_Renderer *renderer;
    SDL_Window *window;
    int quit;

    //Touch variables
    SDL_Point touchLocation;


    /* Inint TTF. */
    SDL_Init(SDL_INIT_TIMER | SDL_INIT_VIDEO);
    // SDL_CreateWindowAndRenderer(WINDOW_WIDTH, WINDOW_WIDTH, SDL_WINDOW_BORDERLESS | SDL_WINDOW_FULLSCREEN_DESKTOP | SDL_WINDOW_MAXIMIZED , &window, &renderer);
    //SDL_CreateWindowAndRenderer(WINDOW_WIDTH, WINDOW_WIDTH, SDL_WINDOW_BORDERLESS | SDL_WINDOW_FULLSCREEN_DESKTOP, &window, &renderer);
    SDL_CreateWindowAndRenderer(WINDOW_WIDTH, WINDOW_WIDTH, 0, &window, &renderer);
    SDL_GetWindowSize(window, &window_x, &window_y);
    touchLocation.x = window_x / 2;
    touchLocation.y = window_y / 2;
    SDL_Log("Window Size x, y:  %d , %d", window_x, window_y);
  
    SDL_Rect r;
    r.x = 0;
    r.y = 0;
    r.w = window_x;
    r.h = 50;
    int what_happens = 0;
        // get_text_and_rect(renderer, 10, 10, "hello, world - from clutter", font, &texture1, &rect1);
    // combine_text(renderer, 10, 10, "hello, world", "hello! world", font, &texture1, &texture2, &rect1, &rect2);
    //get_text_and_rect(renderer, 0, rect1.y + rect1.h, "world", font, &texture2, &rect2);

    // Set render color to blue ( rect will be rendered in this color )
    SDL_SetRenderDrawColor( renderer, 0, 0, 255, 255 );

    // Render rect
    SDL_RenderFillRect( renderer, &r );

    // Render the rect to the screen
    SDL_RenderPresent(renderer);

    quit = 0;
    while (!quit) {
        while (SDL_PollEvent(&event) == 1) {
            what_happens = 0;
            if (event.type == SDL_QUIT) {
                quit = 1;
            }
            //Touch down
            else if( event.type == SDL_FINGERDOWN )
            {
                touchLocation.x = event.tfinger.x * window_x;
                touchLocation.y = event.tfinger.y * window_y;
                SDL_Log("touch");
                what_happens = 1;
                // currentTexture = &gTouchDownTexture;
            }
            //Touch motion
            else if( event.type == SDL_FINGERMOTION )
            {
                touchLocation.x = event.tfinger.x * window_x;
                touchLocation.y = event.tfinger.y * window_y;
                SDL_Log("motion");
                what_happens = 2;
                // currentTexture = &gTouchMotionTexture;
            }
            //Touch release
            else if( event.type == SDL_FINGERUP )
            {
                touchLocation.x = event.tfinger.x * window_x;
                touchLocation.y = event.tfinger.y * window_y;
                SDL_Log("leave");
                what_happens = 3;
                // currentTexture = &gTouchUpTexture;
            }

        }
            // if (is_mouse_button_down){
            //      buttons = SDL_GetGlobalMouseState(&mouse_x, &mouse_y);

            //     if (buttons & SDL_BUTTON(SDL_BUTTON_LEFT))
            //     {
            //         if (mouse_y != old_mouse_y){
            //             for (int i = 0; i < NUMBER_OF_STRING; i++)
            //             {
            //                 rect1[i].y -= (old_mouse_y - mouse_y);
            //                 SDL_Log("click %d , %d", old_mouse_y - mouse_y, mouse_y);
            //             }
            //             old_mouse_y = mouse_y;
            //         }
                    
            //         // SDL_Log("click %d , %d", mouse_x, mouse_y);
            //         // if( mouse_x >= r.x && mouse_x <= r.x + r.w ){
            //         //     if ( mouse_y >= r.y && mouse_y <= r.y + r.h ){
            //         //         SDL_Log("button");
            //         //     }
            //         // }
            //     }
            // }

        // get cursor position relative to window
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 0);
        SDL_RenderClear(renderer);

        if (what_happens == 0){
            SDL_SetRenderDrawColor( renderer, 2, 130, 209, 255 );
        }
        else if (what_happens == 1){
            SDL_SetRenderDrawColor( renderer, 2, 130, 5, 255 );
        }
        else if (what_happens == 2){
            SDL_SetRenderDrawColor( renderer, 102, 130, 160, 255 );
        }
        else if (what_happens == 3){
            SDL_SetRenderDrawColor( renderer, 50, 50, 50, 255 );
        }
        // Render rect
        // r.y = 0;
        SDL_RenderFillRect( renderer, &r );
        // r.y = 50;
        //SDL_SetRenderDrawColor( renderer, 02, 130, 209, 255 );
        // Render rect
        // r.y = clipper.y + (clipper.h / 2);
        // SDL_RenderFillRect( renderer, &r );
        SDL_Rect buttonrect1;
        buttonrect1.x = 0;
        buttonrect1.y = 50;
        buttonrect1.w = 70;
        buttonrect1.h = window_y - buttonrect1.y;
        // SDL_Log("click %d , %d : %d , %d", buttonrect1.x, buttonrect1.y,buttonrect1.w,buttonrect1.h);
        /* Use TTF textures. */
        // SDL_RenderCopy(renderer, texture2, NULL, &rect2);
        SDL_RenderPresent(renderer);
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return EXIT_SUCCESS;
}
