#include <stdlib.h>
#include <stdbool.h>

#include <SDL2/SDL.h>

#define WINDOW_WIDTH 300
// #define WINDOW_HEIGHT (WINDOW_WIDTH)
#define WINDOW_HEIGHT 300


int main(int argc, char **argv) {
    int window_x, window_y;
    SDL_Event event;
    SDL_Renderer *renderer;
    SDL_Window *window;
    int quit;

    /* Inint TTF. */
    SDL_Init(SDL_INIT_VIDEO);
    SDL_CreateWindowAndRenderer(WINDOW_WIDTH, WINDOW_WIDTH, 0, &window, &renderer);
    SDL_GetWindowSize(window, &window_x, &window_y);
    SDL_Log("Window Size x, y:  %d , %d", window_x, window_y);
        // get_text_and_rect(renderer, 10, 10, "hello, world - from clutter", font, &texture1, &rect1);
    // combine_text(renderer, 10, 10, "hello, world", "hello! world", font, &texture1, &texture2, &rect1, &rect2);
    //get_text_and_rect(renderer, 0, rect1.y + rect1.h, "world", font, &texture2, &rect2);

    // Render the rect to the screen
    SDL_RenderPresent(renderer);

    quit = 0;
    while (!quit) {
        while (SDL_PollEvent(&event) == 1) {
            if (event.type == SDL_QUIT) {
                quit = 1;
            }
        }

        // get cursor position relative to window
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 0);
        SDL_RenderClear(renderer);
        // SDL_RenderFillRect( renderer, &r );
        // r.y = 50;
        int x = 0;
        for(x=0;x < window_x; x++){
            // int rand1 = rand() % 256;
            // // int rand2 = rand1;
            // // int rand3 = rand1;
            // int rand2 = rand() % 256;
            // int rand3 = rand() % 256;
            int y = 0;
            SDL_Rect rect;
            for(y=0; y < window_y; y++){
                rect.y = y;
                rect.x = x;
                rect.h = 1;
                rect.w = 1;
                // SDL_SetRenderDrawColor(renderer, rand1, rand2, rand3, 0);
                SDL_SetRenderDrawColor(renderer, rand() % 256, rand() % 256, rand() % 256, 0);
                SDL_RenderFillRect( renderer, &rect );

            }
        }
        /*
        
                for(x=0;x < window_x; x++){
            int y = 0;

            for(y=0; y < window_y; y++){
                rect_array[y][x].y = y;
                rect_array[y][x].x = x;
                rect_array[y][x].h = 1;
                rect_array[y][x].w = 1;
                SDL_SetRenderDrawColor(renderer, 255, 120, 255, 0);
                SDL_RenderFillRect( renderer, &rect_array[y][x] );
            }
        }
        
        */
        //SDL_SetRenderDrawColor( renderer, 02, 130, 209, 255 );
        // Render rect
        // r.y = clipper.y + (clipper.h / 2);
        SDL_RenderPresent(renderer);
        // // wait 1/30th of a second
        // SDL_Delay(1000/30);
        SDL_Delay(1000/4);
    }


    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return EXIT_SUCCESS;
}
